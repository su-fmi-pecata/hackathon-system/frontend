import {FormGroup, ValidationErrors, ValidatorFn} from "@angular/forms";

export class CustomValidators {

}

export function creatDateRangeValidator(startField: string, endField: string): ValidatorFn {
  // @ts-ignore
  return (form: FormGroup): ValidationErrors | null => {

    const start:Date = form.get(startField)?.value;

    const end:Date = form.get(endField)?.value;

    if (start && end) {
      const isRangeValid = (end.getTime() - start.getTime() > 0);

      return isRangeValid ? null : {dateRange:true};
    }

    return null;
  }
}
