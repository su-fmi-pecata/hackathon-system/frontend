import { KeycloakService } from 'keycloak-angular';

export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {
    return () =>
        keycloak.init({
            config: {
                url: 'https://sso.petartoshev.eu/',
                realm: 'hackathon-system',
                clientId: 'local-frontend'
            },
            initOptions: {
                onLoad: 'check-sso',
              // silentCheckSsoRedirectUri: window.location.origin + '/home',
                checkLoginIframe: false,
                checkLoginIframeInterval: 25,
            },
            loadUserProfileAtStartUp: true
        });
}
