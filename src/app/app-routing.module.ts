import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ProfileComponent} from './profile/profile.component';
import {AuthGuard} from './utility/app.guard';
import {HackathonsTableComponent} from "./hackathons/table/hackathons-table.component";
import {HackathonPreviewComponent} from "./hackathons/preview/hackathon-preview.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, data: {title: "Home"}},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {
    path: 'hackathons', data: {title: "Hackathons"}, children: [
      {path: '', component: HackathonsTableComponent},
      {path: ':id', component: HackathonPreviewComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
