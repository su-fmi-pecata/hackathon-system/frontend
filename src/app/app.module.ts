import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';

import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {initializeKeycloak} from './utility/keycloak.init';
import {ProfileComponent} from './profile/profile.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SideNavComponent} from './side-nav/side-nav.component';
import {LayoutModule} from '@angular/cdk/layout';
import {HackathonsTableComponent} from './hackathons/table/hackathons-table.component';
import {CommonModule} from "@angular/common";
import {ProjectFormComponent} from './projects/form/project-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ProjectsTableComponent} from './projects/table/projects-table.component';
import {HackathonFormComponent} from './hackathons/form/hackathon-form.component';
import {MaterialModule} from "./utility/material.module";
import { HackathonPreviewComponent } from './hackathons/preview/hackathon-preview.component';

@NgModule({
  declarations: [
    AppComponent,
    HackathonFormComponent,
    HackathonsTableComponent,
    HomeComponent,
    ProfileComponent,
    ProjectFormComponent,
    ProjectsTableComponent,
    SideNavComponent,
    HackathonPreviewComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    KeycloakAngularModule,
    LayoutModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
