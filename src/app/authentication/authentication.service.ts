import {Injectable} from '@angular/core';
import {AuthGuard} from "../utility/app.guard";
import {KeycloakService} from "keycloak-angular";
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private readonly authGard: AuthGuard,
              private readonly router: Router,
              private readonly keycloakService: KeycloakService) {
  }

  public isAuthenticated(): boolean {
    return this.authGard.isAuthenticated();
  }

  public getUsername(): string {
    return this.keycloakService.getUsername();
  }

  public async logout() {
    await this.authGard.logout();
  }

  public hasAnyRole(requiredRoles: string[]): boolean {
    return this.isAuthenticated()
      && this.keycloakService.getUserRoles(true).filter(requiredRoles.includes).length > 0;
  }

  public async login() {
    // await this.authGard.login(this.router.url);
    await this.authGard.login("/profile"); // fix for some reason
  }

}
