import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Hackathon} from "./hackathon";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HackathonService {

  constructor(private readonly httpClient: HttpClient) {
  }

  public getAll(): Observable<Hackathon[]> {
    return this.httpClient.get<Hackathon[]>(`${environment.baseUrl}/public/hackathons`);
  }

  public register(data: Hackathon): Observable<Hackathon> {
    return this.httpClient.post<Hackathon>(`${environment.baseUrl}/hackathons`, data);
  }
}
