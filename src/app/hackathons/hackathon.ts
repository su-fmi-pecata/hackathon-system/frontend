export interface Hackathon {
  id: number;
  name: string;
  location: string;
  startDate: string;
  endDate: string;
}
