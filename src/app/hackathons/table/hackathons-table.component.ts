import {Component, OnInit, Predicate, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable} from '@angular/material/table';
import {HackathonsTableDatasource} from './hackathons-table-datasource';
import {Hackathon} from "../hackathon";
import {HackathonService} from "../hackathon.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {HackathonFormComponent} from "../form/hackathon-form.component";
import {AuthenticationService} from "../../authentication/authentication.service";
import {MatSnackBar} from "@angular/material/snack-bar";

interface Column {
  key: string;
  sortable: boolean;
}

@Component({
  selector: 'app-hackathons',
  templateUrl: './hackathons-table.component.html',
  styleUrls: ['./hackathons-table.component.scss']
})
export class HackathonsTableComponent
  implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Hackathon>;

  dataSource?: HackathonsTableDatasource;
  private readonly displayedColumns = [
    {key: 'name', sortable: true},
    {key: 'location', sortable: true},
    {key: 'startDate', sortable: true},
    {key: 'endDate', sortable: true},
    {key: 'actions', sortable: false},
  ];

  constructor(private readonly authenticationService: AuthenticationService,
              private readonly hackathonService: HackathonService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadHackathons();
  }

  private loadHackathons(): void {
    this.hackathonService.getAll().subscribe(hackathons => {
      this.dataSource = new HackathonsTableDatasource(hackathons, this.getColumnNames(c => c.sortable));
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
    });
  }

  getColumnNames(columnPredicate: Predicate<Column> = (c => true)): string[] {
    return this.displayedColumns.filter(columnPredicate).map(column => column.key);
  }

  openCreateFormDialog(): void {
    if(!this.authenticationService.isAuthenticated())
    {
      let snackBarRef = this.snackBar.open('You have to be logged in to register a hackathon!');
      return;
    }

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    this.dialog.open(HackathonFormComponent, dialogConfig);
  }

  registerNewProject(): void{

  }

}
