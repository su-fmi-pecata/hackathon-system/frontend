import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {creatDateRangeValidator} from "../../utility/custom-validators";
import {HackathonService} from "../hackathon.service";


@Component({
  selector: 'app-hackathon-form',
  templateUrl: './hackathon-form.component.html',
  styleUrls: ['./hackathon-form.component.scss']
})
export class HackathonFormComponent {
  form = this.fb.group({
    name: [null, Validators.required],
    location: [null, Validators.required],
    startDate: [null, Validators.required],
    endDate: [null, Validators.required],
  }, {
    validators: [creatDateRangeValidator("startDate", "endDate")]
  });

  constructor(private fb: FormBuilder,
              private readonly hackathonService: HackathonService) {
  }

  onSubmit(): void {
    this.hackathonService.register(this.form.value)
      .subscribe(res => window.location.reload());
  }
}
