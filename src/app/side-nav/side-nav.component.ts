import {Component} from '@angular/core';
import {AuthenticationService} from "../authentication/authentication.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent {

  constructor(public readonly router: Router,
              public readonly authenticationService: AuthenticationService) {
  }

}
